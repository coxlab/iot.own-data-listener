import io from 'socket.io-client';

if (process.argv.length != 5) {
  console.error('IoT.own URL, User ID and token value must be specified as parameters.');
  console.error('Usage: npm start {IoT.own URL} {user ID} {token}');
  process.exit(1);
}

const server = process.argv[2];
const userId = process.argv[3];
const token = process.argv[4];

const socket = io(server, { forceNew: true });
socket.open();

socket.on('connect', () => {
  console.log(socket.connected);
  if (socket.connected) {
    console.log('Socket connected');
    socket.emit('joinroom', { userId: userId, token: token });
  } else {
    console.log('Socket connect failed');
    process.exit(2);
  }
});

socket.on('data', (nid, last_data, timestamp) => {
  console.log(`NID: ${nid}, last_data: ${JSON.stringify(last_data)}, timestamp: ${timestamp}`);
});
